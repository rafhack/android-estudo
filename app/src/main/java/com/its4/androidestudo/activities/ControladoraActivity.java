package com.its4.androidestudo.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.its4.androidestudo.R;

import fragments.Test1Fragment;
import interfaces.Teste1Callback;

/**
 * Created by root on 1/23/16.
 */
public class ControladoraActivity extends AppCompatActivity implements Teste1Callback {

    private Button btnEnviar;
    private EditText edtInfo;
    private TextView tvwCallback;

    private Test1Fragment fragmentTest1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_fragments);

        btnEnviar = (Button) findViewById(R.id.activity_test_fragment_btn_enviar);
        edtInfo = (EditText) findViewById(R.id.activity_test_fragments_edt_info);
        tvwCallback = (TextView) findViewById(R.id.activity_test_fragments_tvw_callback);

        fragmentTest1 = new Test1Fragment();
        fragmentTest1.setCallback(this);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.activity_test_fragments_frm_fragments, fragmentTest1);
        ft.commit();

        setListeners();
    }

    private void setListeners() {
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTest1.setTextoInfo(edtInfo.getText().toString());
            }
        });
    }

    @Override
    public void botao1clicado(String info) {
        tvwCallback.setText(info);
    }
}
