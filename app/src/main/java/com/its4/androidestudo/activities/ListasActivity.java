package com.its4.androidestudo.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.TextView;

import com.its4.androidestudo.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by root on 1/30/16.
 */
public class ListasActivity extends AppCompatActivity {

    private TextView tvwConteudo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listas);

        tvwConteudo = (TextView) findViewById(R.id.activity_listas_tvw_conteudo);

        //region ArrayList -----------
        ArrayList<String> listaStrings = new ArrayList<>();

        listaStrings.add("H");
        listaStrings.add("G");
        listaStrings.add("B");
        listaStrings.add("U");
        listaStrings.add("V");

        Collections.reverse(listaStrings);

        tvwConteudo.setText(TextUtils.join(",", listaStrings));
        //endregion

        //region Array -------------
        String[] arrayStrings = new String[5];
        arrayStrings[0] = "A";
        arrayStrings[3] = "B";

        //tvwConteudo.setText(Arrays.toString(arrayStrings));
        //endregion

        //region HashSet -------------
        HashSet<Integer> codigosPeople = new HashSet<>();

        codigosPeople.add(3);
        codigosPeople.add(5);
        codigosPeople.add(3);
        codigosPeople.add(6);
        codigosPeople.add(7);

        ArrayList<Integer> arrayAPartirDoSet = new ArrayList<>(codigosPeople);
        arrayAPartirDoSet.get(3);

        tvwConteudo.setText(TextUtils.join(",", codigosPeople.toArray()));

        //endregion

        //region Map ------------

        HashMap<Integer, String> mapNomes = new HashMap<>();

        mapNomes.put(6, "João");
        mapNomes.put(8, "Maria");

        mapNomes.containsKey(4);
        mapNomes.remove(6);

        tvwConteudo.setText(mapNomes.get(8));

        //endregion

        //region JSON


        //region Criar JSON na mão
        try {
            JSONObject object = new JSONObject();

            JSONObject jsonPessoa = new JSONObject();
            jsonPessoa.put("Nome", "João");
            jsonPessoa.put("Idade", "44");

            JSONArray arrayPaises = new JSONArray();

            arrayPaises.put("Brasil");
            arrayPaises.put("Canada");
            arrayPaises.put("Colombia");

            object.put("pesssoa", jsonPessoa);
            object.put("paises", arrayPaises);
            object.put("Id", 5);

            //tvwConteudo.setText(object.getJSONObject("pesssoa").getString("Nome"));
            //tvwConteudo.setText(Integer.toString(object.getInt("Id")));
            //tvwConteudo.setText(arrayPaises.getString(1));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //endregion


        //region JSON String


        try {

            String respostaServidor = "{\"array\": [1,2,3],\"boolean\": true,\"null\": null,\"number\": 123,\"object\": {\"a\": \"b\",\"c\": \"d\",\"e\": \"f\"},\"string\": \"Hello World\"}";

            JSONObject jsonServidor = new JSONObject(respostaServidor);
            JSONArray arrayServidor = jsonServidor.getJSONArray("array");

            tvwConteudo.setText(Integer.toString(arrayServidor.getInt(1)));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //endregion


        //endregion
    }

}
