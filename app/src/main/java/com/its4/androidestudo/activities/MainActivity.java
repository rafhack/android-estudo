package com.its4.androidestudo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.its4.androidestudo.R;

public class MainActivity extends AppCompatActivity {

    private Button btnAbrirTela;
    private Button btnAbrirTelaFragments;
    private Button btnAbrirTelaListas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        btnAbrirTela = (Button) findViewById(R.id.activity_princial_btn_abrir_tela);
        btnAbrirTelaFragments = (Button) findViewById(R.id.activity_principal_btn_abrir_tela_fragments);
        btnAbrirTelaListas = (Button) findViewById(R.id.activity_principal_btn_abrir_tela_listas);

        setListeners();
    }

    private void setListeners() {

        btnAbrirTelaListas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(MainActivity.this, ListasActivity.class);
                startActivity(intent);
            }
        });

        btnAbrirTela.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("BOTAO", "Fui clicado!!");
                Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        btnAbrirTelaFragments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ControladoraActivity.class);
                startActivity(intent);
            }
        });

    }

}