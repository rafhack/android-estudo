package com.its4.androidestudo.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.its4.androidestudo.R;

import dialogs.OlaMundoDialogFragment;

/**
 * Created by root on 1/16/16.
 */
public class RegisterActivity extends AppCompatActivity {

    private Button btnFecharTela;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);

        btnFecharTela = (Button) findViewById(R.id.register_activity_btn_fechar_tela);
        setListeners();
    }

    private void setListeners() {
        btnFecharTela.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                finish();
                return true;
            }
        });

        btnFecharTela.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OlaMundoDialogFragment dialog = new OlaMundoDialogFragment();
                dialog.show(getSupportFragmentManager(), "Dialog");
            }
        });
    }
}
