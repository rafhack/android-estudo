package dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.its4.androidestudo.R;

/**
 * Created by root on 1/16/16.
 */
public class OlaMundoDialogFragment extends AppCompatDialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(R.string.fechar);
        getDialog().setCanceledOnTouchOutside(false);
        View view = inflater.inflate(R.layout.ola_mundo_dialog_fragment, container, false);

        return view;
    }
}
