package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.its4.androidestudo.R;

import interfaces.Teste1Callback;

/**
 * Created by root on 1/23/16.
 */
public class Test1Fragment extends Fragment {

    private TextView tvwInfo;
    private Button btnCallback;

    private Teste1Callback callback;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_test_1, container, false);

        tvwInfo = (TextView) view.findViewById(R.id.fragment_test_1_tvw_info);
        btnCallback = (Button) view.findViewById(R.id.fragment_test_1_btn_callback);

        setListeners();

        return view;
    }

    private void setListeners() {
        btnCallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.botao1clicado("BOTAO DO FRAGMENT CLICADO!!");
            }
        });
    }

    /**
     *
     * Este metodo define o texto do TextView do fragment
     *
     * @param text o texto para ser mostrado
     */
    public void setTextoInfo(String text) {
        tvwInfo.setText(text);
    }

    public void setCallback(Teste1Callback callback) {
        this.callback = callback;
    }
}
